// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import firebase from 'firebase'
import App from './App'
import router from './router'
import { store } from './store/store'

Vue.config.productionTip = false

// firabase initialization
var config = {
  apiKey: 'AIzaSyBmTjn9s33cJTrAf1Fe7hwN0QWHz_7v-Ko',
  authDomain: 'cinemath-cda4a.firebaseapp.com',
  databaseURL: 'https://cinemath-cda4a.firebaseio.com',
  projectId: 'cinemath-cda4a',
  storageBucket: 'cinemath-cda4a.appspot.com',
  messagingSenderId: '458598760993'
}

firebase.initializeApp(config)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
