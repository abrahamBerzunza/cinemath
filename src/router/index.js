import Vue from 'vue'
import Router from 'vue-router'
import Billboard from '@/components/Billboard'
import Premieres from '@/components/Premieres'
import Building from '@/components/Building'
import Room from '@/components/Room'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', redirect: '/billboard' },
    { path: '/billboard', name: 'Billboard', component: Billboard },
    { path: '/premieres', name: 'Premieres', component: Premieres },
    { path: '/presales', name: 'Building', component: Building },
    { path: '/promotions', name: 'Building', component: Building },
    { path: '/room/:id/:name', name: 'Room', component: Room }
  ]
})
