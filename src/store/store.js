import Vue from 'vue'
import Vuex from 'vuex'
import firebase from 'firebase'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    user: null
  },
  getters: {
    getUser: (state) => {
      return state.user
    }
  },
  mutations: {
    setUser: (state, user) => {
      state.user = user
    }
  },
  actions: {
    signIn ({ commit }) {
      const provider = new firebase.auth.GoogleAuthProvider()

      firebase.auth().signInWithPopup(provider)
        .then((result) => {
          commit('setUser', result.user)
        })
        .catch((error) => {
          console.log(error)
        })
    },
    signOut ({ commit }) {
      firebase.auth().signOut()
        .then(() => {
          // Sign-out successful.
          commit('setUser', null)
        }).catch((error) => {
          // An error happened.
          console.log(error)
        })
    }
  }
})
